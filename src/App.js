import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
// import Register from "./register/Register.js";
import Login from "./login/Login.js";
import Admin from "./admin/Admin.js";
import AdminTable from "./AdminTable/AdminTable.js";
import UpdateUser from "./UpdateUser/UpdateUser.js";
import HomePage from "./homePage/HomePage.js";
import Register from "./register/Register.js";
import Muellimler from "./muellimler/Muellimler.js";
import AddTeacher from "./addTeacher/AddTeacher.js";
import Comments from "./comments/Comments.js"

function App() {
  return (
    <BrowserRouter>
      <Routes>
        {/* <Route path="/register" element={<Register />} /> */}
        <Route path="/login" element={<Login />} />
        <Route path="/admin" element={<Admin />} />
        <Route path="/adminTable" element={<AdminTable />} />
        <Route path="/edit/user/:id" element={<UpdateUser />} />
        <Route path="/home" element={<HomePage />} />
        <Route path="/register" element={<Register />} />
        <Route path="/teachers" element={<Muellimler />} />
        <Route path="/addTeacher" element={<AddTeacher />} />
        <Route path="/comment/all" element={<Comments/>}/>
      </Routes>
    </BrowserRouter>
  );
}
export default App;
