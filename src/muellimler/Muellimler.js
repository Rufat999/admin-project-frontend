import React from "react";
import axios from "axios";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import AzTULogo from "./AzTULogo.png";
import Logo from "./Logo.jpg";

const Muellimler = () => {
  const [teachers, setTeachers] = useState([]);
  useEffect(() => {
    loadTeachers();
  }, []);

  const loadTeachers = async () => {
    const result = await axios.get(`http://localhost:9099/teacher/all`);
    setTeachers(result.data);
  };

  return (
    <div>
      <div className="container1" style={{ backgroundColor: "#0F52BA" }}>
        <div className="row">
          <div
            className="col-xl-5 col-lg-5 col-md-5 col-sm-12 r_panel_div wow fadeInUp"
            style={{
              visibility: "visible",
              textAlign: "left",
            }}
          >
            <h2
              className="text_white"
              style={{
                width: "510px",
                color: "white",
                marginLeft: "380px",
                marginTop: "20px",
                fontFamily: "'Roboto Condensed', sans-serif",
              }}
            >
              Kompüter Texnologiyaları kafedrası
            </h2>
          </div>
          <div>
            <img
              style={{
                position: "absolute",
                top: "3px",
                left: "170px",
                height: "55px",
                borderRadius: "5px",
              }}
              src={AzTULogo}
              alt=""
              width="165"
              height="197"
            />
          </div>
          <div>
            <img
              style={{
                position: "absolute",
                top: "3px",
                right: "170px",
                height: "60px",
                borderRadius: "5px",
              }}
              src={Logo}
              alt=""
              width="165"
              height="197"
            />
          </div>
        </div>
      </div>
      <div className="teachers">
        <div style={{
          fontSize: "30px",
          fontWeight: "bold"
        }}>Əməkdaşlar</div>
        {/* Emekdaslar olan hisse */}
        <br></br>
        <div>
            {teachers.map((teacher) => (
              <div key={teacher.id}>
              <h6><Link to={`/comment/all`}>{teacher.teacher}</Link></h6>
              </div>
            ))}
        </div>
      </div>
    </div>
  );
};
export default Muellimler;
