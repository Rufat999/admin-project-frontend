import axios from "axios";
import { useState, useEffect, Component } from "react";
import { Link, useHistory, useParams } from "react-router-dom";

const Comments = () => {
  const [comments, setComments] = useState([]);
  useEffect(() => {
    loadComments();
  }, []);

  const loadComments = async () => {
    const result = await axios.get(`http://localhost:9099/comment/all`);
    setComments(result.data);
    console.log(result.data);
  };

  return (
    <div>
      {comments.map((comment) => (
        <div key={comment.id}>
          <h6>{comment.id}. {comment.comment}</h6>
        </div>
      ))}
    </div>
  );
};
export default Comments;
