import axios from "axios";
import { useState, useEffect, Component } from "react";
import { Link, useHistory, useParams } from "react-router-dom";
import UpdateUserCss from "./UpdateUserCss.css";
import KafedraIcon2 from "./KafedraIcon2.jpg";
import AzTUIcon from "./AZTU.png";
import KOICAIcon from "./KOICA.png";

const UpdateUser = () => {
  const { id } = useParams();

  const [data, setData] = useState({
    surname: "",
    name: "",
    email: "",
    password: "",
  });

  const handleChange = (e) => {
    const value = e.target.value;
    setData({
      ...data,
      [e.target.name]: value,
    });
  };

  useEffect(() => {
    loadUser();
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    const bookData = {
      surname: data.surname,
      name: data.name,
      email: data.email,
      password: data.password,
    };
    axios
      .put(`http://localhost:9099/user/${id}/update`, bookData)
      .then((response) => {
        console.log(response);
        window.location.href = "http://localhost:3000/adminTable";
      });
  };
  const loadUser = async (e) => {
    const result = await axios.get(`http://localhost:9099/user/${id}`);
    setData(result.data);
  };

  return (
    <div>
      <div class="row justify-content-center">
        <div class="col-md-7 col-lg-5">
          <div class="login-wrap p-4 p-md-5">
            <div class="row mb-4">
              <div class="col-4">
                <img src={AzTUIcon} height="60" />
              </div>
              <div class="col-4">
                <img src={KafedraIcon2} height="60" />
              </div>
              <div class="col-4">
                <img src={KOICAIcon} class="float-right" height="60" />
              </div>
            </div>
            <h4 class="text-center mb-4">Update</h4>
            <form>
              <div class="form-group d-flex">
                <input
                  class="form-control rounded-left"
                  type="text"
                  name="surname"
                  id="surname"
                  value={data.surname}
                  onChange={handleChange}
                />
              </div>
              <div class="form-group d-flex">
                <input
                  class="form-control rounded-left"
                  type="text"
                  name="name"
                  id="name"
                  value={data.name}
                  onChange={handleChange}
                />
              </div>
              <div class="form-group d-flex">
                <input
                  class="form-control rounded-left"
                  type="text"
                  name="email"
                  id="email"
                  value={data.email}
                  onChange={handleChange}
                />
              </div>
              <div class="form-group d-flex">
                <input
                  class="form-control rounded-left"
                  type="password"
                  name="password"
                  id="password"
                  value={data.password}
                  onChange={handleChange}
                />
              </div>
              <div class="form-group">
                <button
                  type="submit"
                  class="form-control btn btn-primary rounded submit px-3"
                  onClick={handleSubmit}
                >
                  Update
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};
export default UpdateUser;
