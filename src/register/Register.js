import axios from "axios";
import { useState, useEffect, Component } from "react";
import { Link, useHistory, useParams } from "react-router-dom";
import RegisterCss from "./RegisterCss.css";
import KafedraIcon2 from "./KafedraIcon2.jpg";
import AzTUIcon from "./AZTU.png";
import KOICAIcon from "./KOICA.png";

const Register = () => {
  const [data, setData] = useState({
    surname: "",
    name: "",
    email: "",
    password: "",
  });

  const handleChange = (e) => {
    const value = e.target.value;
    setData({
      ...data,
      [e.target.name]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const userData = {
      surname: data.surname,
      name: data.name,
      email: data.email,
      password: data.password,
    };
    axios
      .post("http://localhost:9099/user/register", userData)
      .then((response) => {
        window.location.href = "http://localhost:3000/login";
        console.log(response.data.message);
      })
      .catch((error) => {
        if (error.response.status === 400) {
          alert(error.response.data);
        }
      });
  };

  return (
    <div>
      <div class="row justify-content-center">
        <div class="col-md-7 col-lg-5">
          <div class="login-wrap p-4 p-md-5">
            <div class="row mb-4">
              <div class="col-4">
                <img src={AzTUIcon} height="60" />
              </div>
              <div class="col-4">
                <img src={KafedraIcon2} height="60" />
              </div>
              <div class="col-4">
                <img src={KOICAIcon} class="float-right" height="60" />
              </div>
            </div>
            <h4 class="text-center mb-4">Qeydiyyatdan keçin</h4>
            <form>
              <div class="form-group d-flex">
                <input
                  style={{
                    height: "52px",
                    background: "#fff",
                    color: "#000",
                    fontSize: "16px",
                    borderRadius: "5px",
                    border: "1px solid rgba(0, 0, 0, 0.1)",
                  }}
                  class="form-control rounded-left"
                  type="text"
                  name="surname"
                  id="surname"
                  value={data.surname}
                  onChange={handleChange}
                  placeholder="Soyad"
                />
              </div>
              <div class="form-group d-flex">
                <input
                  style={{
                    height: "52px",
                    background: "#fff",
                    color: "#000",
                    fontSize: "16px",
                    borderRadius: "5px",
                    border: "1px solid rgba(0, 0, 0, 0.1)",
                    marginTop: "0.5em",
                  }}
                  class="form-control rounded-left"
                  type="text"
                  name="name"
                  id="name"
                  value={data.name}
                  onChange={handleChange}
                  placeholder="Ad"
                />
              </div>
              <div class="form-group d-flex">
                <input
                  style={{
                    height: "52px",
                    background: "#fff",
                    color: "#000",
                    fontSize: "16px",
                    borderRadius: "5px",
                    border: "1px solid rgba(0, 0, 0, 0.1)",
                    marginTop: "0.5em",
                  }}
                  class="form-control rounded-left"
                  type="text"
                  name="email"
                  id="email"
                  value={data.email}
                  onChange={handleChange}
                  placeholder="Email"
                />
              </div>
              <div class="form-group d-flex">
                <input
                  style={{
                    height: "52px",
                    background: "#fff",
                    color: "#000",
                    fontSize: "16px",
                    borderRadius: "5px",
                    border: "1px solid rgba(0, 0, 0, 0.1)",
                    marginTop: "0.5em",
                  }}
                  class="form-control rounded-left"
                  type="password"
                  name="password"
                  id="password"
                  value={data.password}
                  onChange={handleChange}
                  placeholder="Parol"
                />
              </div>
              <div class="form-group">
                <button
                  style={{
                    height: "52px",
                    background: "#1E90FF",
                    color: "white",
                    fontSize: "15px",
                    borderRadius: "0.25rem !important;",
                    border: "1px solid",
                    marginTop: "1em",
                  }}
                  type="submit"
                  class="form-control btn btn-primary rounded submit px-3"
                  onClick={handleSubmit}
                >
                  Qeydiyyatdan keçin
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Register;
