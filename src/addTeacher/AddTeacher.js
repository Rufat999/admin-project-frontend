import axios from "axios";
import { useState, useEffect, Component } from "react";
import { Link, useHistory, useParams } from "react-router-dom";
import KafedraIcon2 from "./images/KafedraIcon2.jpg";
import AzTUIcon from "./images/AZTU.png";
import KOICAIcon from "./images/KOICA.png";

const AddTeacher = () => {
  const [data, setData] = useState({
    teacher: "",
  });

  const handleChange = (e) => {
    const value = e.target.value;
    setData({
      ...data,
      [e.target.name]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const teacherData = {
      teacher: data.teacher,
    };
    axios
      .post("http://localhost:9099/teacher/add", teacherData)
      .then((response) => {
        window.location.href = "http://localhost:3000/admin";
        console.log(response.data.message);
      })
      .catch((error) => {
        if (error.response.status === 400) {
          alert(error.response.data);
        }
      });
  };

  return (
    <div>
      <div class="row justify-content-center">
        <div class="col-md-7 col-lg-5">
          <div class="login-wrap p-4 p-md-5">
            <div class="row mb-4">
              <div class="col-4">
                <img src={AzTUIcon} height="60" />
              </div>
              <div class="col-4">
                <img src={KafedraIcon2} height="60" />
              </div>
              <div class="col-4">
                <img src={KOICAIcon} class="float-right" height="60" />
              </div>
            </div>
            <h4 class="text-center mb-4">Müəllim əlavə edin!</h4>
            <form>
              <div class="form-group d-flex">
                <input
                  style={{
                    height: "52px",
                    background: "#fff",
                    color: "#000",
                    fontSize: "16px",
                    borderRadius: "5px",
                    border: "1px solid rgba(0, 0, 0, 0.1)",
                  }}
                  class="form-control rounded-left"
                  type="text"
                  name="teacher"
                  id="teacher"
                  value={data.teacher}
                  onChange={handleChange}
                  placeholder="Müəllim"
                />
              </div>
              <div class="form-group">
                <button
                  style={{
                    height: "52px",
                    background: "#1E90FF",
                    color: "white",
                    fontSize: "15px",
                    borderRadius: "0.25rem !important;",
                    border: "1px solid",
                    marginTop: "1em",
                  }}
                  type="submit"
                  class="form-control btn btn-primary rounded submit px-3"
                  onClick={handleSubmit}
                >
                  Əlavə et
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};
export default AddTeacher;
