import "bootstrap/dist/css/bootstrap.min.css";
import SimpleImageSlider from "react-simple-image-slider";
import React from "react";
import { MDBCarousel, MDBCarouselItem } from "mdb-react-ui-kit";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image1 from "./images/1.jpg";
import Image2 from "./images/2.jpg";
import Image3 from "./images/3.jpg";
import Image4 from "./images/4.jpg";
import Image5 from "./images/5.png";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import "@splidejs/react-splide/css/skyblue";
import { Slide } from "react-slideshow-image";
import "react-slideshow-image/dist/styles.css";
import TeacherIcon from "./images/Icon colmd1 teacher.jpg";
import HeydərƏliyevin100İlliyi from "./images/Heydər Əliyevin 100 illiyi.png";
import AzTU0 from "./images/AzTU0.jpg";
import AzTU1 from "./images/AzTU1.jpg";
import AzTU2 from "./images/AzTU2.jpg";
import AzTUIcon from "./images/AzTUIcon.jpg";
import Xeber1 from "./images/Xeber1.jpg";
import FifthSection from "./images/5-ci hisse.png";
import StyleCss from "./StyleCss.css";

const HomePage = () => {
  const spanStyle = {
    padding: "20px",
    background: "#efefef",
    color: "#000000",
  };

  const divStyle = {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundSize: "cover",
    height: "400px",
  };

  const images = [AzTU2, AzTU0, AzTU1];

  return (
    <div>
      <div class="navbar navbar-light bg-dark navbar-expand-md px-3">
        <a href="#" class="navbar-brand">
          <img
            className="kafedraninLogosu"
            src={AzTUIcon}
            alt=""
            width="50px"
            height="50px"
            ezimgfmt="rs rscb1 src ng ngcb1"
            loading="eager"
            srcset=""
            sizes=""
            importance="high"
            fetchpriority="high"
          />
        </a>
        <div class="navbar-collapse collapse" id="menu">
          <ul class="navbar-nav ms-auto">
            <li class="nav-item mx-3">
              <a href="#" class="nav-link" style={{ color: "white" }}>
                Haqqımızda
              </a>
            </li>
            <li class="nav-item mx-3">
              <a href="#" class="nav-link" style={{ color: "white" }}>
                Fəaliyyətlər
              </a>
            </li>
            <li class="nav-item mx-3">
              <a href="#" class="nav-link" style={{ color: "white" }}>
                Tədbirlər
              </a>
            </li>
            <li class="nav-item mx-3">
              <a
                href="http://localhost:3000/teachers"
                class="nav-link"
                style={{ color: "white" }}
              >
                Müəllimlər
              </a>
            </li>
            <li class="nav-item mx-3">
              <a
                href="http://localhost:3000/comment/all"
                class="nav-link"
                style={{ color: "white" }}
              >
                Şərhlər
              </a>
            </li>
            <li class="nav-item mx-3">
              <a
                href="http://localhost:3000/login"
                class="nav-link"
                style={{ color: "white" }}
              >
                KOİCA
              </a>
            </li>
          </ul>
        </div>
      </div>

      <div className="slide-container">
        <SimpleImageSlider
          width={1232}
          height={521}
          images={images}
          showNavs={true}
        />
      </div>

      <br></br>
      <br></br>
      <br></br>

      <div class="third-area">
        <div class="container1">
          <div class="row f_panel_boxes">
            <a
              href="http://localhost:3000/teachers"
              class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-4 wow fadeIn f_panel_box"
              target="_blank"
              style={{
                visibility: "visible",
                animationDuration: "0.5s",
                animationDelay: "0.05s",
                animationName: "fadeIn",
                overflowClipMargin: "content-box",
                overflow: "clip",
                textAlign: "center",
              }}
              data-wow-duration="0.5s"
              data-wow-delay=".05s"
            >
              <div class="f_box_div">
                <div class="colmd-box-icon">
                  <img
                    class="f_panel_img"
                    style={{}}
                    src={TeacherIcon}
                    alt="..."
                  />
                </div>
                <h4> Müəllimlər </h4>
              </div>
            </a>

            <a
              href="#"
              class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-4 wow fadeIn f_panel_box"
              target="_blank"
              style={{
                visibility: "visible",
                animationDuration: "0.5s",
                animationDelay: "0.05s",
                animationName: "fadeIn",
                textAlign: "center",
              }}
              data-wow-duration="0.5s"
              data-wow-delay=".05s"
            >
              <div class="f_box_div">
                <div class="colmd-box-icon">
                  <img
                    class="f_panel_img"
                    src="https://www.aztu.edu.az/assets/dist/img/f_panel/k.svg"
                    alt="..."
                  />
                </div>
                <h4> Kitabxana </h4>
              </div>
            </a>

            <a
              href="#"
              class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-4 wow fadeIn f_panel_box"
              target="_blank"
              style={{
                visibility: "visible",
                animationDuration: "0.5s",
                animationDelay: "0.05s",
                animationName: "fadeIn",
                textAlign: "center",
              }}
              data-wow-duration="0.5s"
              data-wow-delay=".05s"
            >
              <div class="f_box_div">
                <div class="colmd-box-icon">
                  <img
                    class="f_panel_img"
                    src="https://www.aztu.edu.az/assets/dist/img/f_panel/t.svg"
                    alt="..."
                  />
                </div>
                <h4>Əcnəbi tələbələr</h4>
              </div>
            </a>

            <a
              href="#"
              class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-4 wow fadeIn f_panel_box"
              target="_blank"
              style={{
                visibility: "visible",
                animationDuration: "0.5s",
                animationDelay: "0.05s",
                animationName: "fadeIn",
                textAlign: "center",
              }}
              data-wow-duration="0.5s"
              data-wow-delay=".05s"
            >
              <div class="f_box_div">
                <div class="colmd-box-icon">
                  <img
                    class="f_panel_img"
                    src="https://www.aztu.edu.az/assets/dist/img/f_panel/oi.svg"
                    alt="..."
                  />
                </div>
                <h4>Onlayn imtahan</h4>
              </div>
            </a>

            <a
              href="#"
              class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-4 wow fadeIn f_panel_box"
              target="_blank"
              style={{
                visibility: "visible",
                animationDuration: "0.5s",
                animationDelay: "0.05s",
                animationName: "fadeIn",
                textAlign: "center",
              }}
              data-wow-duration="0.5s"
              data-wow-delay=".05s"
            >
              <div class="f_box_div">
                <div class="colmd-box-icon">
                  <img
                    class="f_panel_img"
                    src="https://www.aztu.edu.az/assets/dist/img/f_panel/oa.svg"
                    alt="..."
                  />
                </div>
                <h4>Onlayn apelyasiya</h4>
              </div>
            </a>

            <a
              href="#"
              class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-4 wow fadeIn f_panel_box f_panel_box_r_border"
              target="_blank"
              style={{
                visibility: "visible",
                animationDuration: "0.5s",
                animationDelay: "0.05s",
                animationName: "fadeIn",
                textAlign: "center",
              }}
              data-wow-duration="0.5s"
              data-wow-delay=".05s"
            >
              <div class="f_box_div">
                <div class="colmd-box-icon">
                  <img
                    class="f_panel_img"
                    src="https://www.aztu.edu.az/assets/dist/img/f_panel/d.svg"
                    alt="..."
                  />
                </div>
                <h4>Distant təhsil</h4>
              </div>
            </a>
          </div>
        </div>
      </div>

      <br></br>
      <br></br>
      <br></br>

      <div class="r_panel_w">
        <div class="container1" style={{ backgroundColor: "rgba(0,51,102)" }}>
          <div class="row">
            <div
              class="col-xl-5 col-lg-5 col-md-5 col-sm-12 r_panel_div wow fadeInUp"
              style={{
                visibility: "visible",
                animationDuration: "0.5s",
                animationDelay: "0.2s",
                animationName: "fadeInUp",
                textAlign: "left",
                dataWowDuration: ".5s",
                dataWowDelay: ".20s",
              }}
            >
              <h2 class="text_white" style={{ textAlign: "left" }}>
                &nbsp;
              </h2>
              <h2
                class="text_white"
                style={{
                  textAlign: "left",
                  color: "white",
                  marginLeft: "60px",
                  marginTop: "120px",
                  fontFamily: "'Roboto Condensed', sans-serif",
                  marginBlockStart: "0.83em",
                  marginBlockEnd: "0.83em",
                }}
              >
                Heydər Əliyev ili - 2023
              </h2>
              <p>&nbsp;</p>
            </div>
            <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 ">
              <div
                class="r_panel_img"
                style={{
                  backgroundAttachment: "scroll",
                  backgroundPosition: "right bottom",
                  backgroundRepeat: "no-repeat",
                  backgroundSize: "100%",
                  marginTop: "0px",
                }}
              >
                <img
                  src={HeydərƏliyevin100İlliyi}
                  alt=""
                  width="465"
                  height="397"
                />
              </div>
            </div>
          </div>
        </div>
      </div>

      <br></br>

      <div className="xeberler">
        <div className="row">
          <div class="col-md-6">
            <img className="xeber1" src={AzTU0} />
          </div>
          <div className="col-md-3">
            <ol style={{ color: "white", backgroundColor: "#002147" }}>
              <li>
                Aprel Prezident İlham Əliyev və Prezident Rəcəb Tayyib Ərdoğan
                İstanbulda “TEKNOFEST” ...
              </li>
              <li>
                Aprel Prezident İlham Əliyev və Prezident Rəcəb Tayyib Ərdoğan
                İstanbulda “TEKNOFEST” ...
              </li>
              <li>
                Aprel Prezident İlham Əliyev və Prezident Rəcəb Tayyib Ərdoğan
                İstanbulda “TEKNOFEST” ...
              </li>
              <li>
                Aprel Prezident İlham Əliyev və Prezident Rəcəb Tayyib Ərdoğan
                İstanbulda “TEKNOFEST” ...
              </li>
              <br></br>
            </ol>
          </div>
          <div class="col-md-3">
            <img className="xeber1" src={AzTU2} />
          </div>
        </div>
      </div>

      <div
        style={{
          with: "100%",
          height: "5px",
          backgroundColor: "#eee",
          marginTop: "30px",
        }}
      ></div>

      <div>
        <img style={{ width: "1240px" }} src={FifthSection} />
      </div>

      <br></br>
      <br></br>

      <div class="container2">
        <div class="row">
          <div class="col-6 col-xl-6 col-lg-12 col-md-12 col-sm-12 text_left d-none d-sm-block">
            <h3 class="area_h_title">Layihələr</h3>
          </div>

          <div class="col-md-6">
            <a
              href="#"
              class="t_m_color all_view_link"
            >
              <img
                src="https://www.aztu.edu.az/assets/dist/img/tools/nnb.svg"
                alt="product"
              />
              Bütün layihələr
            </a>
          </div>
        </div>

        {/* 6 hisse */}
        <div className="altinciHisse">
          <div className="row">
            <div
              className="col-md-3"
              data-wow-duration="0.5s"
              data-wow-delay=".05s"
            >
              <a href="#" class="yazi">
                <div class="imageAltinciHisse">
                  <img
                    className="imageAltinciHisse"
                    src={AzTU2}
                    alt="Card image cap"
                  />
                  <div class="card-body">
                    <b class="card-title">Sabah Qrupları layihəsi... </b>
                    <p class="card-text-hisse">Niyə SABAH?</p>
                  </div>
                </div>
              </a>
            </div>

            <div
              className="col-md-3"
              data-wow-duration="0.5s"
              data-wow-delay=".05s"
            >
              <a href="#" class="yazi">
                <div class="imageAltinciHisse">
                  <img
                    className="imageAltinciHisse"
                    src={AzTU2}
                    alt="Card image cap"
                  />
                  <div class="card-body">
                    <b class="card-title">Sabah Qrupları layihəsi... </b>
                    <p class="card-text-hisse">Niyə SABAH?</p>
                  </div>
                </div>
              </a>
            </div>

            <div
              className="col-md-3"
              data-wow-duration="0.5s"
              data-wow-delay=".05s"
            >
              <a href="#" class="yazi">
                <div className="imageAltinciHisse">
                  <img
                    class="imageAltinciHisse"
                    src={AzTU2}
                    alt="Card image cap"
                  />
                  <div class="card-body">
                    <b class="card-title">Sabah Qrupları layihəsi... </b>
                    <p class="card-text-hisse">Niyə SABAH?</p>
                  </div>
                </div>
              </a>
            </div>

            <div
              className="col-md-3"
              data-wow-duration="0.5s"
              data-wow-delay=".05s"
            >
              <a href="#" className="yazi">
                <div class="imageAltinciHisse">
                  <img
                    className="imageAltinciHisse"
                    src={AzTU2}
                    alt="Card image cap"
                  />
                  <div class="card-body">
                    <b class="card-title">Sabah Qrupları layihəsi... </b>
                    <p class="card-text-hisse">Niyə SABAH?</p>
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>

      <br></br>

      {/* Footer */}

      <div className="footer">
        <div className="row">
          <div className="col-md-4">
            <img
              style={{
                marginTop: "20px",
                marginBottom: "20px",
                marginLeft: "20px",
                borderRadius: "5px",
              }}
              src={AzTUIcon}
            />
          </div>
          <div className="col-md-4">
            <p
              style={{
                textAlign: "center",
                marginTop: "50px",
                fontSize: "15px",
                fontFamily: "Roboto Condensed', sans-serif",
              }}
            >
              H.Cavid prospekti 25, Bakı, Azərbaycan AZ 1073 Azərbaycan Texniki
              Universiteti.
            </p>
          </div>
          <div
            className="col-md-4"
            style={{
              textAlign: "center",
              marginTop: "40px",
              fontSize: "15px",
              fontFamily: "Roboto Condensed', sans-serif",
            }}
          >
            <p>Tel: (+994 12) 538-33-83</p>
            <p>Qaynar xətt: (+994 12) 539-13-05</p>
            <p>E-poçt: aztu@aztu.edu.az</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HomePage;
