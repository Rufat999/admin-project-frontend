import React from "react";
import axios from "axios";
import { useState, useEffect } from "react";
import AdminCss from "./AdminCss.css";
import ProfileIcon from "./Profile Icon.png";

export default function Admin() {
  return (
    <div>
      <div className="sidebar">
        <img className="profileIcon" src={ProfileIcon}></img>
        <ul>
          <li className="active">
            <a href="#">
              <i className="fa-solid fa-chart-column"></i>
              <span>İdarə Paneli</span>
            </a>
          </li>
          <li>
            <a href="http://localhost:3000/adminTable">
              <i className="fa-solid fa-building-user"></i>
              <span>Admin Cədvəli
              </span>
            </a>
          </li>
          <li>
            <a href="http://localhost:3000/register">
              <i className="fa-regular fa-file-lines"></i>
              <span style={{textAlign: "left"}}>Yeni Admin Yarat</span>
            </a>
          </li>
          <li>
            <a href="http://localhost:3000/addTeacher">
              <i className="fa-solid fa-building-user"></i>
              <span style={{textAlign: "left"}}>Yeni Müəllim Yarat</span>
            </a>
          </li>
        </ul>
      </div>
      <div className="content">
        <nav>
          <div className="profile-img">
            <img
              src="https://cdn.vectorstock.com/i/preview-1x/10/78/laptop-computer-with-tech-icons-vector-25871078.webp"
              alt="profile img"
            />
          </div>
        </nav>

        <div className="containerAdmin">
          <header>
            <h1>Xoş gəldiniz, Hörmətli Admin</h1>
          </header>
        </div>
      </div>
    </div>
  );
}
