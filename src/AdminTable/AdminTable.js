import React from "react";
import axios from "axios";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import AdminTableCss from "./AdminTableCss.css";

const AdminTable = () => {
  const [users, setUsers] = useState([]);
  useEffect(() => {
    loadUsers();
  }, []);

  const loadUsers = async () => {
    const result = await axios.get(`http://localhost:9099/user/all`);
    setUsers(result.data);
  };

  const deleteUserById = async (id) => {
    var result = window.confirm("Are you sure to delete?");
    if (result) {
      await axios.delete(`http://localhost:9099/user/${id}`);
      loadUsers();
    }
  };

  const [query, setQuery] = useState("");
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchUsers = async () => {
      const res = await axios.get(
        `http://localhost:9099/user/search/all?query=${query}`
      );
      setData(res.data);
      console.log(res.data)
    };
    fetchUsers();
  }, [query]);

  return (
    <div className="bodyAdminTable">
      <div className="container">
        <a href="http://localhost:3000/register">
          <button className="yarat">Yeni Admin Əlavə Et</button>
        </a>
        <form action="#" className="filter">
          <input
          className="axtar"
            type="search"
            name="filtter"
            id="filtter"
            placeholder="Axtar"
            onChange={(e) => setQuery(e.target.value)}
          />
          {/* <button>filtter</button> */}
        </form>
        <table>
          <thead>
            <tr>
              <th>Id</th>
              <th>Soyad</th>
              <th>Ad</th>
              <th>Email</th>
              <th>Parol</th>
            </tr>
          </thead>
          <tbody>
            {users.map((user) => (
              <tr key={user.id}>
                <td>{user.id}</td>
                <td>{user.surname}</td>
                <td>{user.name}</td>
                <td>{user.email}</td>
                <td>{user.password}</td>
                <td>
                  <Link className="link" to={`/edit/user/${user.id}`}>
                    Edit
                  </Link>
                  <button
                    className="deleteButton"
                    onClick={() => deleteUserById(user.id)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};
export default AdminTable;
